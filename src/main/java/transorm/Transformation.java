package transorm;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Properties;

public class Transformation {


    public static void main(String argv[]) throws TransformerException, IOException {
//        System.setProperty("javax.xml.transform.TransformerFactory", "org.apache.xalan.xsltc.trax.TransformerFactoryImpl");
        Properties p = System.getProperties();
        TransformerFactory transformerFactory;
        Templates templates ;

        transformerFactory = TransformerFactory.newInstance();

        String nombreFichTempDest =  "/home/javalderas/Escritorio/salidaTransformada.xml";
        String pathIN = "/home/javalderas/Descargas/CM10BaseTest.xml";
        String pathXslt = "/home/javalderas/Descargas/plantilla.xslt";
        File xmlfile = new File(pathIN);
        File outfile = new File(nombreFichTempDest);

        // creamos la plantilla en si
        File xslt = new File(pathXslt);
        xslt.createNewFile();
        byte[] xsltbytes = Files.readAllBytes(xslt.toPath());

        Source xsltSource = new StreamSource(new ByteArrayInputStream(xsltbytes));
        templates = transformerFactory.newTemplates(xsltSource);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        // preparamos la entrada
        Source xmlSource = new StreamSource(xmlfile);
        // preparamos la salida
        //Result result = new StreamResult(outfile);
        Result result = new StreamResult(outfile);

        // creamos un transformador y hacemos la transformacion
        Transformer transformer = templates.newTransformer();
        transformer.setErrorListener(new ErrorListener() {
            public void warning(TransformerException exception) throws TransformerException {

            }

            public void error(TransformerException exception) throws TransformerException {

            }

            public void fatalError(TransformerException exception) throws TransformerException {

            }
        });

//        if(this.getParametros()!=null && this.getParametros().size()>0){
//            String nombre="";
//            for (Enumeration enumer = this.getParametros().keys(); enumer.hasMoreElements();){
//                nombre=(String)enumer.nextElement();
//                transformer.setParameter(nombre, this.getParametros().get(nombre));
//            }
//        }

        transformer.transform(xmlSource, result);

    }


}
